/*
Module Dependencies 
*/
var express = require('express'),
    http = require('http'),
    path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    uriUtil = require('mongodb-uri'),
    hash = require('./pass').hash;

var app = express();

/*
Database and Models
*/
var options = { server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, 
                replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } } };  
var mongodbUri ='mongodb://localhost/aewedding';
var mongooseUri = uriUtil.formatMongoose(mongodbUri);
mongoose.connect(process.env.MONGOLAB_URI || mongooseUri, options);
var db = mongoose.connection;

//Set up Controllers
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  // hao qiu!
  fs.readdirSync(__dirname+'/server/controllers').forEach(function(file)
  {
    if(file.substr(-3) == '.js')
    {
      route = require(__dirname+'/server/controllers/' + file);
      route.controller(app);
    }
  });
});


/*
Middlewares and configurations 
*/
app.configure(function () {
    app.set('port', (process.env.PORT || 5000));
    app.use(express.bodyParser());
    app.use(express.cookieParser('Authentication Tutorial '));
    app.use(express.session());
    app.use(express.static(path.join(__dirname, 'public')));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(require('stylus').middleware({ src: __dirname + '/public' }));
    app.use(express.static(__dirname + '/public'));
});

app.use(function (req, res, next) {
    var err = req.session.error,
        msg = req.session.success;
    delete req.session.error;
    delete req.session.success;
    res.locals.message = '';
    if (err) res.locals.message = '<p class="msg error">' + err + '</p>';
    if (msg) res.locals.message = '<p class="msg success">' + msg + '</p>';
    next();
});

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
});