//1 is accept, 0 is decline
function uncheck(resp){
	var ACCEPT=1;
	var DECLINE=0;
	var acceptElem=document.getElementById("accept");
	var declineElem=document.getElementById("decline");
	if(resp===ACCEPT){	
		declineElem.checked = false;
		declineElem.required = false;
		acceptElem.required = true;
	}else if(resp===0){
		acceptElem.checked = false;
		acceptElem.required = false;
		declineElem.required = true;
	}
}