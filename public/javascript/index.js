function checkPassword(){
	var password=document.getElementById("password").value;
	var rsvpPass="annie&emerson";
	var guestListPass="AE2016";
	var errElem=document.getElementById("ErrorMsg");
	if(errElem){
		errElem.remove();
	}
	if(password===guestListPass){
		//Redirect to GuesList
		window.location="/guestlist";

	}
	else if(password===rsvpPass){
		//Redirect to RSVP Page
		window.location="/rsvpform";

	}else{
		//popup Error
		var rsvpElem=document.getElementById("rsvpPopup");
		var p = document.createElement("P");
		p.innerHTML="* Incorrect Password";
		p.id="ErrorMsg";
		p.style.color="rgb(140,140,140)";
		//rsvpElem.insertBefore(p, rsvpElem.childNodes[0]); // append before
		rsvpElem.appendChild(p); // append after

	}
}

function overlay() {
	var el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}

function hidePopup(id){
	var x = document.getElementById(id);
	x.style.visibility="hidden";
	x.style.opacity=0;
}
function showPopup(id){
	var x = document.getElementById(id);
	x.style.visibility="visible";
	x.style.opacity=1;
}