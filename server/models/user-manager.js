var dbName = 'users';
var mongoose = require('mongoose');
var userSchema = mongoose.Schema;

var UserSchema = new mongoose.Schema({
    password: String,
    lastname: String,
    firstname: String,
    mailingaddress: String,
    numadults: Number,
    numkids: Number,
    acceptdecline: String,
    comments: String,
});

var User = mongoose.model(dbName, UserSchema);

//User info: dictionary with keys username and password
exports.addUser=function(userInfo, callback)
{
	/*TODO: check if username taken */
	var user = new User(userInfo);
	user.save(function (err) {
        if (err)
        {
        	console.log("Error: " + err);
        	return callback(err, {message:'ERROR'});
        }
        else
        	return callback(null, {message:'OK'})
    });

}

exports.deleteUser=function(id, callback)
{
    User.remove({_id:id}, function(err){
        if(err){
            return callback(err, {message:'ERROR'});
        }
        else
            return callback(null, {message:'OK'});
    });
}

exports.getAllUsers=function(callback)
{
    User.find({}, function(err, docs) {
       if(err) return callback(err,docs);
     //  console.log(JSON.stringify(docs));
       return callback(err,docs);
    });
}

exports.deleteAll=function(callback)
{
    mongoose.connection.collections[dbName]
    .drop( function(err) { 
        if(err)
            callback(err, null);
        else
            callback(null, {message:'OK'});
    });
}