var UM = require("../models/user-manager.js");

module.exports.controller = function(app){
	app.get("/", function(req,res){
		res.render("index");
	});

	app.get("/login", function (req, res) {
		var password = req.param('password');
		console.log(password);
		if( password === "annie&emerson" ) {
			res.render("rsvpform");
			console.log("Access Granted.");
		}
		else if( password === "AE2016" ) {
			UM.getAllUsers(function(err,users){
				if(err){
					res.send('Users can not be found', 402);
					return handleError(err);
				}
				res.render("guestlist", {users: users});
			});
		}
		else {
			console.log("Access Denied.");
		}
	});

	app.get("/rsvpform", function (req, res) {
		res.render("rsvpform");
	});

	app.get("/signup", function (req, res) {
	    res.render("signup");
	});

	app.get("/guestlist", function (req, res) {
		var totalAdults = 0;
		var totalKids = 0;

		UM.getAllUsers(function(err,users){
	 		if(err){
				res.send('Users can not be found', 402);
				return handleError(err);
			}
			users.sort(function(a,b){
				if(a.lastname < b.lastname) return -1;
    			if(a.lastname > b.lastname) return 1;
    			return 0;
    		});

			console.log(users);
			users.forEach(function(user){
				user.lastname = user.lastname?user.lastname.toUpperCase():user.lastname;
				if(user.acceptdecline == "Accepted"){
					console.log("apple");
					totalAdults = totalAdults + parseInt(user.numadults);
					totalKids = totalKids + parseInt(user.numkids);
				}
			});
			users.sort(function(a,b){
				if(a.lastname < b.lastname) return -1;
    			if(a.lastname > b.lastname) return 1;
    			return 0;
    		});
			res.render("guestlist", {users: users, totalAdults: totalAdults, totalKids: totalKids});
		});
	});

	app.post("/signup", function (req, res) {
		var lastname = req.body.lastName;
		var firstname = req.body.firstName;
		var mailingaddress = req.body.MailingAddress;
		var numAdults = req.body.numberAdults;
		var numKids = req.body.numberKids;
		var accept = req.body.Accepts;
		var decline = req.body.Declines;
		var comments = req.body.Comments;
		var acceptDecline;
		var errormsg = "* Please fill out all required fields *";

		if( accept ) {
			acceptDecline = accept;
		}else if( decline ) {
			acceptDecline = decline;
		}

		if( !lastname || !firstname || !mailingaddress || (!accept && !decline)) {
			console.log("hao qiu");
			res.render("rsvpform", {error: errormsg});
		}else{
		    var userInfo = {"lastname": lastname, "firstname": firstname, "mailingaddress": mailingaddress, 
		    				"numadults": numAdults, "numkids": numKids,  "acceptdecline": acceptDecline, "comments": comments};
		    UM.addUser(userInfo, function(err, msg){
		    	if(err){
					res.render("rsvpform", {error: "* Error submitting form. Please resubmit."});
		    	}else{
		    		res.render("thankYouPage");
		    	}
		    });
		}
    });

    app.get("/rsvpformerror", function (req, res){
    	res.render("rsvpformerror");
    })

    app.get("/thankYouPage", function (req, res) {
    	res.render("thankYouPage");
    });

	app.get('/users', function(req, res){
	    UM.getAllUsers(function(err, users){
	        if(err){
	            res.send('Users can not be found', 402);
	            return handleError(err);
	        }
	        res.send(users);
	    });
	});

	app.post('/deleteUser', function(req, res){
		var id = req.body.userID;
		var totalAdults = 0;
		var totalKids = 0;

		UM.deleteUser(id, function(err, msg){
	    	if(err){
	    		console.log(err);
	    		res.send({'msg':err});
	    	}else{
		    	UM.getAllUsers(function(err,users){
			 		if(err){
						res.send('Users can not be found', 402);
						return handleError(err);
					}
					users.forEach(function(user){
						user.lastname = user.lastname.toUpperCase();
						console.log(user.lastname);
						if(user.acceptdecline == "Accepted"){
							totalAdults = totalAdults + parseInt(user.numadults);
							totalKids = totalKids + parseInt(user.numkids);
						}
					});
					users.onkeyup = function(){
					    this.value = this.value.toUpperCase();
					}
					users.sort(function(a,b){
						if(a.lastname < b.lastname) return -1;
		    			if(a.lastname > b.lastname) return 1;
		    			return 0;
		    		});
					res.render("guestlist", {users: users, totalAdults: totalAdults, totalKids: totalKids});
				});
	    	}
		});
	});
	/*app.get('/testUsers', function(req, res){
		for(var i=0; i<400; i++){
		    var userInfo = {"lastname": "lastname"+i, "firstname": "firstname"+i, "mailingaddress": "mailingaddress"+i, 
		    				"numadults": i, "numkids": i,  "acceptdecline": "acceptDecline"+i, "comments": "comments"+i};
		    UM.addUser(userInfo, function(err, msg){

		    });
		}
		res.send('OK');
	});
	app.get('/deleteall', function(req, res){
		UM.deleteAll(function(err, users){
			if(err){
				res.send('Cannot delete users', 402);
				return handleError(err);
			}
			res.send('OK');
		});
	});*/

}